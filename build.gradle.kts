import org.taruts.djigGradlePlugin.credentials.impl.GradleProjectPropertiesCredentialsProvider
import org.taruts.djigGradlePlugin.extension.DjigPluginExtension
import org.taruts.djigGradlePlugin.extension.Visibility
import org.taruts.djigGradlePlugin.propertyWriting.enums.PropertyType
import org.taruts.djigGradlePlugin.propertyWriting.enums.PropertyWritingPlace
import org.taruts.gitLabContainerGradlePlugin.GitLabContainerPluginExtension
import org.taruts.workspaceGradlePlugin.WorkspacePluginExtension
import java.net.URL

plugins {
    id("org.taruts.workspace") version "1.0.3"
    id("org.taruts.gitlab-container") version "1.0.0"
    id("org.taruts.djig") version "1.0.0"
}

group = "org.taruts.djig"

configure<GitLabContainerPluginExtension> {
    url.set(URL("http://localhost:9580"))
    username.set("user")
    password.set("123456789")
}

configure<DjigPluginExtension> {

    app {
        sourcesRelativePath.set("projects/example-app")
        workingDirectory.set(rootDir.resolve("projects/example-app/working-directory"))
    }

    sourceDynamicProjects {
        springBootProfile.set("dynamic-dev")
        gitHub {
            groupsSeparator.set(".")
        }
    }

    task("dynamic-local") {

        sourceDynamicProjects {
            credentialsProvider.set(GradleProjectPropertiesCredentialsProvider())
        }

        target.publicGitHub {
            hosting {
                credentialsProvider.set(GradleProjectPropertiesCredentialsProvider())
            }
            visibility.set(Visibility.PRIVATE)
            gitHub {
                groupsSeparator.set(".")
            }

            preferredFileExtension.set("yml")

            projectPropertyWriteStrategy {
                location.set { _, _ -> "file:./config/" }
            }

            credentialsPropertyWriteStrategy {
                place.set(PropertyWritingPlace.APP_PROPERTIES_RESOURCE)
                propertyType.set(PropertyType.PER_HOSTING)
                location.set { _, _ -> "file:./config/" }
            }
            branchPropertyWriteStrategy {
                place.set(PropertyWritingPlace.APP_PROPERTIES_RESOURCE)
                propertyType.set(PropertyType.COMMON)
                location.set { _, _ -> "file:./config/" }
            }
        }
        transform {
            djigProjectNameToSourcePathFunction.set { "dynamic-local-$it" }
            transformNamespaceExpression.set("(pavel-taruts>)/**")
            hostingProjectNameTransformer.set {
                it.replace("\\bdev\\b".toRegex(), "local")
            }
        }
    }
}

configure<WorkspacePluginExtension> {
    gitHub {
        groupSeparator.set("-")
    }
}
